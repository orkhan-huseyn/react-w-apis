import request from './instance';

export async function getProducts(skip, limit, signal) {
  const response = await request.get(`/products`, {
    params: { skip, limit },
    signal,
  });
  return response.data;
}

export async function getProduct(id, signal) {
  const response = await request.get(`/products/${id}`, {
    signal,
  });
  return response.data;
}
