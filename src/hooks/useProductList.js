import { useEffect, useState } from 'react';
import { getProducts } from '../api/products';

const LIMIT = 10;

export function useProductList() {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadingMore, setLoadingMore] = useState(true);
  const [error, setError] = useState('');
  const [skip, setSkip] = useState(0);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    const controller = new AbortController();

    setLoadingMore(true);

    getProducts(skip, LIMIT, controller.signal)
      .then((res) => {
        setError('');
        setProducts((products) => [...products, ...res.products]);
        setTotal(res.total);
      })
      .catch((error) => {
        setError(error.message);
      })
      .finally(() => {
        setLoading(false);
        setLoadingMore(false);
      });

    return () => {
      controller.abort();
    };
  }, [skip]);

  function handleLoadMore() {
    setSkip((skip) => skip + LIMIT);
  }

  return [products, loading, loadingMore, error, total, handleLoadMore];
}
