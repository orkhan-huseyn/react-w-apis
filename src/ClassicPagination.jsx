import { useEffect, useState } from 'react';
import Pagination from './Pagination';

const LIMIT = 10;

function ClassicPagination() {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState('');
  const [skip, setSkip] = useState(0);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    fetch(`https://dummyjson.com/products?skip=${skip}&limit=${LIMIT}`)
      .then((res) => {
        if (!res.ok) {
          return Promise.reject('Something went wrong');
        }
        return res;
      })
      .then((res) => res.json())
      .then((res) => {
        setProducts(res.products);
        setTotal(res.total);
      })
      .catch((error) => {
        setError(error);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [skip]);

  function handlePageChange(page) {
    const skip = (page - 1) * LIMIT;
    setSkip(skip);
  }

  if (error) {
    return <span className="alert alert-danger">{error}</span>;
  }

  if (loading) {
    return <h2>Loading...</h2>;
  }

  return (
    <div className="container">
      <ul className="list-group">
        {products.map((product) => (
          <li className="list-group-item" key={product.id}>
            {product.title}
          </li>
        ))}
      </ul>
      <Pagination
        onPageChange={handlePageChange}
        total={total}
        pageSize={LIMIT}
      />
    </div>
  );
}

export default ClassicPagination;
