import { useProductList } from './hooks/useProductList';

function LoadMorePagination() {
  const [products, loading, loadingMore, error, total, handleLoadMore] =
    useProductList();

  if (error) {
    return <span className="alert alert-danger">{error}</span>;
  }

  if (loading) {
    return <h2>Loading...</h2>;
  }

  return (
    <div className="container">
      <ul className="list-group">
        {products.map((product) => (
          <li className="list-group-item" key={product.id}>
            {product.title}
          </li>
        ))}
      </ul>
      {total > products.length ? (
        <button
          disabled={loadingMore}
          onClick={handleLoadMore}
          className="btn btn-primary"
        >
          {loadingMore ? 'Loading...' : 'Load more'}
        </button>
      ) : null}
    </div>
  );
}

export default LoadMorePagination;
