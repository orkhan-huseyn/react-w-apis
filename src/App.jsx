import { useState } from 'react';
import LoadMorePagination from './LoadMorePagination';

function App() {
  const [mount, setMount] = useState(true);

  function handleClick() {
    setMount((m) => !m);
  }

  return (
    <>
      <button onClick={handleClick}>{mount ? 'Unmount' : 'Mount'}</button>
      {mount ? <LoadMorePagination /> : null}
    </>
  );
}

export default App;
